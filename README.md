# PyTorchUbuntuDocker

This repository uses the gitlab pipeline to build a Docker image that [builds PyTorch from source](https://gist.github.com/mhubii/1c1049fb5043b8be262259efac4b89d5).


&nbsp;

## Getting docker 

On Mac the best is to install the [Docker Desktop](https://docs.docker.com/desktop/mac/install/). (The docker installed by `brew` has knowns issues.) You may need to select a previous version depending on the age of your OS. 


On Ubuntu there are [many ways](https://docs.docker.com/engine/install/ubuntu/).


&nbsp;

## Get the local docker image 

1) Getting image:
```
$ sudo docker pull registry.gitlab.com/bothambrus/pytorchubuntudocker:latest
```

2) Try the container:
```
$ sudo docker run  --privileged=true --volume ~:/my_home -it registry.gitlab.com/bothambrus/pytorchubuntudocker
# ls
bin  boot  dev  etc  home  lib  lib32  lib64  libx32  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
# ls root/pytorch/torch
CMakeLists.txt    _linalg_utils.py           _tensor_str.py      cpu                    fx               onnx              share
README.txt        _lobpcg.py                 _torch_docs.py      csrc                   hub.py           optim             sparse
_C                _lowrank.py                _utils.py           cuda                   include          overrides.py      special
_VF.py            _masked                    _utils_internal.py  custom_class.h         jit              package           storage.py
_VF.pyi           _namedtensor_internals.py  _vmap_internals.py  custom_class_detail.h  legacy           profiler          test
__config__.py     _ops.py                    abi-check.cpp       deploy.h               lib              py.typed          testing
__future__.py     _python_dispatcher.py      ao                  distributed            library.h        quantization      torch_version.py
__init__.py       _six.py                    autocast_mode.py    distributions          linalg           quasirandom.py    types.py
_appdirs.py       _sources.py                autograd            extension.h            monitor          random.py         utils
_classes.py       _storage_docs.py           backends            fft                    multiprocessing  return_types.py   version.py
_deploy.py        _tensor.py                 bin                 functional.py          nested           script.h
_jit_internal.py  _tensor_docs.py            contrib             futures                nn               serialization.py
# exit
$
```
Option `-i` starts an interactive session, i.e.: docker will read the standard input, while option `-t` puts the standard output and error on the terminal. 


3) Displaying locally available images:
```
$ sudo docker image ls
REPOSITORY                                           TAG                 IMAGE ID            CREATED             SIZE
registry.gitlab.com/bothambrus/pytorchubuntudocker   latest              d651282aa3bc        10 hours ago        9.31GB
...
```

4) Displaying locally available containers:
```
$ sudo docker container ls -a
CONTAINER ID        IMAGE                                                COMMAND               CREATED             STATUS                      PORTS               NAMES
3753774970cf        registry.gitlab.com/bothambrus/pytorchubuntudocker   "bash"                38 minutes ago      Exited (0) 20 minutes ago                       infallible_ritchie
...
```


&nbsp;

## Creating singularity image from docker image

Based on this [article](https://www.nas.nasa.gov/hecc/support/kb/converting-docker-images-to-singularity-for-use-on-pleiades_643.html):
```
$ singularity pull pv_singularity.sif docker://registry.gitlab.com/bothambrus/pytorchubuntudocker:latest
```

Alternatively you can try [`docker2singularity`](https://github.com/singularityhub/docker2singularity) if `singularity` itself is not available.


This is advantegous for running the pytorch c++ programs on for example MareNostrum, where the only container option is singularity. 



&nbsp;

## Miscellaneous

The gitlab pipeline building this docker image is a copy from [Best practices for building docker images with GitLab CI](https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/).







